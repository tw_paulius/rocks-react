import Navigation from './Navigation';

import logo from './logo.jpeg';

function Header() {
    return (
        <header>
            <div className="row--dark">
                <div className="header-top-row contain">
                    <a href="/" className="logo" title="Rocks logo">
                        <img src={logo} alt="Rocks logo"/>
                    </a>
                    <div className="search">
                        <form action="" method="post">
                            <input type="search" name="keyword"/>
                        </form>
                    </div>
                    <button className="login" title="Click here to login">Login</button>
                </div>
            </div>
            <div className="row">
                <Navigation />
            </div>
        </header>
    );
}

export default Header;
