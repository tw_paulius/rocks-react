import {useState, useEffect} from "react";

// TODO: fix 'if api is down, map dies with error'
const mockApiUrl = 'http://localhost:3035/menu';

function Navigation() {

    const [data, setData] = useState([]);
    const [isFetching, setIsFetching] = useState(true);

    useEffect(() => {
        const fetchLinksAsync = async () => {
            try {
                const response = await fetch(mockApiUrl);
                const jsonData = await response.json();
                setData(jsonData);
                setIsFetching(false);
            } catch (e) {
                console.log(e);
                setIsFetching(false);
                setData([]);
            }
        }
        fetchLinksAsync();
    }, []);

    const Link = (props) => {
        return (
            <a href={props.url} className="nav-link" title={props.title}>{props.title}</a>
        );
    }

    const navList = data.map(link =>
        <Link {...link} key={link.id} />
    );

    return (
        <nav className="contain" id="mainNavigation">
            {isFetching ? 'Loading...' : navList }
        </nav>
    );
}

export default Navigation;
