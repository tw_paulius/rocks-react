import Header from './Header/Header';
import Carousel from './Carousel/Carousel';
import ProductsList from './Products/ProductsList';
import Footer from './Footer/Footer';

function App() {
    return (
        <div>
            <Header />
            <Carousel />
            <ProductsList />
            <Footer />
        </div>
    );
}

export default App;
