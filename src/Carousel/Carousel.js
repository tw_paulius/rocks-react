import {useState, useEffect} from "react";
import Slide from './Slide';

import iconChevron from './icon-chevron-up.svg';
const mockApiUrl = 'http://localhost:3035/products?featured=true';

function Carousel() {

    const [data, setData] = useState([]);
    const [isFetching, setIsFetching] = useState(true);

    useEffect(() => {
        const fetchProductsAsync = async () => {
            try {
                const response = await fetch(mockApiUrl);
                const jsonData = await response.json();
                setData(jsonData);
                setIsFetching(false);
            } catch (e) {
                console.log(e);
                setIsFetching(false);
                setData([]);
            }
        }
        fetchProductsAsync();
    }, []);

    const [currentSlideIndex, setCurrentSlideIndex] = useState(0);

    const slideLeftHandler = (event) => {
        event.preventDefault();
        if (currentSlideIndex === 0) {
            setCurrentSlideIndex(data.length - 1);
        } else {
            setCurrentSlideIndex(currentSlideIndex - 1);
        }
    }

    const slideRightHandler = (event) => {
        event.preventDefault();
        if (currentSlideIndex === (data.length - 1)) {
            setCurrentSlideIndex(0);
        } else {
            setCurrentSlideIndex(currentSlideIndex + 1);
        }
    }

    const currentSlide = <Slide {...data[currentSlideIndex]} index={currentSlideIndex} className="active" />;

    return (
        <section className="hero row--dark">
            <div className="carousel" data-carousel>
                <button
                    className="carousel_arrow carousel_arrow--left"
                    onClick={e => {slideLeftHandler(e)}}
                    title="Previous"
                >
                    <img src={iconChevron} alt="Previous"/>
                </button>
                <div className="carousel__track" id="carouselSlides">
                    {isFetching ? 'Loading...' : currentSlide}
                </div>
                <button
                    className="carousel_arrow carousel_arrow--right"
                    onClick={e => {slideRightHandler(e)}}
                    title="Next"
                >
                    <img src={iconChevron} alt="Next"/>
                </button>
            </div>
        </section>
    );
}

export default Carousel;
