
function Slide (props) {
    const className = props.className ? `carousel__slide ${props.className}` : 'carousel__slide';

    return (
        <div className={className} shit={props.index}>
            <img src={`/images/carousel/${props.image}`}
                 alt={props.title}
                 className="carousel__slide-image"
            />
                <a href={`/product-${props.id}`} className="carousel__slide-link" title={props.title}>
                    <div className="carousel__slide-content">
                        <div className="carousel__slide-header">{props.title}</div>
                        {props.body}
                    </div>
                </a>
        </div>
    );
}

export default Slide;
