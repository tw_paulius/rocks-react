
function Product(props) {
    return (
        <li className="products-list__item">
            <a
                href={`/product-${props.id}`}
                className="products-list__item-link"
                title={props.title}
                onClick={(e) => props.editProductHandler(e, props.id)}
            >
                <img
                    src={`/images/products/${props.image}`}
                    className="products-list__item-image"
                    alt={props.title}
                />
                <div className="products-list__item-name">{props.name}</div>
            </a>
        </li>
    );
}

export default Product;
