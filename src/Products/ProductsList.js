import {useState, useEffect} from "react";

import Product from './Product';
import ProductForm from './ProductForm';

const mockApiUrl = 'http://localhost:3035/products';

function ProductsList() {

    const [data, setData] = useState([]);
    const [isFetching, setIsFetching] = useState(true);

    const [triggerReFetch, setTriggerReFetch] = useState(false);

    useEffect(() => {
        const fetchProductsAsync = async () => {
            try {
                const response = await fetch(mockApiUrl);
                const jsonData = await response.json();
                setData(jsonData);
                setIsFetching(false);
            } catch (e) {
                console.log(e);
                setIsFetching(false);
                setData([]);
            }
        }
        fetchProductsAsync();
    }, [triggerReFetch]);

    const [productBeingEdited, setProductBeingEdited] = useState(0);
    const [formAction, setFormAction] = useState('');
    const [formOpen, setFormOpen] = useState(false);
    const closeFormHandler = () => {
        setFormOpen(false);
        setFormAction('');
        setFormData(formDataReset);
        setProductBeingEdited(0);
    }
    const newProductHandler = () => {
        setFormOpen(true);
        setFormAction('new');
    }

    const formDataReset = {name:'', title:'', body: '', image: '', featured: false};
    const [formData, setFormData] = useState(formDataReset);
    const formDataHandler = (e) => {
        const value = e.target.type === "checkbox" ? e.target.checked : e.target.value;
        setFormData({
            ...formData,
            [e.target.name]: value
        });
    }

    const submitFormHandler = (e) => {
        e.preventDefault();
        if (e.nativeEvent.submitter.name === 'delete') {
            //TODO: confirm removal
            const fetchData = {
                method: 'DELETE',
                headers: {'Content-Type': 'application/json'}
            }
            fetch(mockApiUrl + '/' + productBeingEdited, fetchData)
                .then(response => response.json())
                .then(function(ok) {
                    //console.log(ok);
                    setProductBeingEdited(0);
                    setTriggerReFetch(flipMe => !flipMe);
                    setFormData(formDataReset);
                    setFormOpen(false);
                }).catch(function (error) {
                    console.log(error);
                });
        } else if (formAction === 'edit') {
            const fetchData = {
                method: 'PUT',
                body: JSON.stringify(formData),
                headers: {'Content-Type': 'application/json'}
            }
            fetch(mockApiUrl + '/' + productBeingEdited, fetchData)
                .then(response => response.json())
                .then(function(ok) {
                    //console.log(ok);
                    setProductBeingEdited(0);
                    setTriggerReFetch(flipMe => !flipMe);
                    setFormData(formDataReset);
                    setFormOpen(false);
                }).catch(function (error) {
                    console.log(error);
                });
        } else if (formAction === 'new') {
            let image = Math.floor(Math.random() * (8 - 1) + 1);
            let data = {
                ...formData,
                "image": image + ".jpg"
            };
            const fetchData = {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
            }
            fetch(mockApiUrl, fetchData)
                .then(response => response.json())
                .then(function() {
                    setTriggerReFetch(flipMe => !flipMe);
                    setFormData(formDataReset);
                    setFormOpen(false);
                }).catch(function (error) {
                    console.log(error);
                });
        }
    }

    const editProductHandler = (e, productId) => {
        e.preventDefault();
        fetch(mockApiUrl + '/' + productId).then((response) => response.json()).then(function(data) {
            setFormOpen(true);
            setFormAction('edit');
            setFormData(data);
            setProductBeingEdited(productId);
        }).catch(function (error) {
            console.log(error);
        });
    }

    const productForm =
        <ProductForm
            closeFormHandler={closeFormHandler}
            submitFormHandler={submitFormHandler}
            formData={formData}
            formDataHandler={formDataHandler}
            formAction={formAction}
        />
    ;

    const productList = data.map(product =>
        <Product {...product}
                 key={product.id}
                 editProductHandler={editProductHandler}
        />
    );

    return (
        <>
            <section className="row products">
                <ul className="products-list contain" id="productsList">
                    {isFetching ? 'Loading...' : productList}
                </ul>
            </section>
            <section className="row contain">
                <button id="addNewProductTrigger" className="btn" onClick={newProductHandler}>Add new product</button>
            </section>
            {formOpen && productForm}
        </>
    );
}

export default ProductsList;
