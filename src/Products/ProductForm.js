
function ProductForm(props) {
    function Edit() {
        return (
            <>
                <button id="addNewProduct" className="btn" name="save">Save</button>
                <button id="removeProduct" className="btn" name="delete">Delete</button>
            </>
        );
    }

    function New() {
        return (
            <>
                <button id="addNewProduct" className="btn" name="save">Save</button>
            </>
        );
    }

    const actions = props.formAction === 'edit' ? <Edit /> : <New />;
    return (
        <div className="new-product" id="addNewProductForm">
            <button className="close btn" onClick={props.closeFormHandler}>X</button>
            <form onSubmit={props.submitFormHandler}>
                <label className="label" htmlFor="newName">Name</label>
                <input type="text"
                       id="newName"
                       name="name"
                       value={props.formData.name}
                       onChange={props.formDataHandler}
                       required />

                <label className="label" htmlFor="newTitle">Title</label>
                <input type="text"
                       id="newTitle"
                       name="title"
                       value={props.formData.title}
                       onChange={props.formDataHandler}
                       required />

                <label className="label" htmlFor="newDescription">Description</label>
                <textarea name="body"
                          id="newDescription"
                          value={props.formData.body}
                          onChange={props.formDataHandler} />

                <div className="label">
                    <input type="checkbox"
                           name="featured"
                           id="newFeatured"
                           checked={props.formData.featured}
                           onChange={props.formDataHandler} />
                    <label htmlFor="newFeatured">Featured</label>
                </div>
                {actions}
                <input type="hidden" name="product_action" id="newAction" value="none"/>
                <input type="hidden" name="product_id" id="updateId" value=""/>
                <input type="hidden" name="image" value={props.formData.image} onChange={props.formDataHandler} />
            </form>
        </div>
    );
}

export default ProductForm;
